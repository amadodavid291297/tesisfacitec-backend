export interface IRuc {
    _id?: string;
    ruc?: string;
    nombre?: string;
    dv?: string;
    ruc2?: string;
}