"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const ruc_model_1 = require("../models/ruc_model");
const rucRoutes = (0, express_1.Router)();
rucRoutes.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let pagina = Number(req.query.pagina) || 1;
    let salto = Number(req.query.salto) || 18;
    let skip = pagina - 1;
    skip = skip * salto;
    const ruc = yield ruc_model_1.Ruc.find()
        .sort({ _id: -1 })
        .skip(skip)
        .limit(salto)
        .exec();
    res.json({
        ok: true,
        pagina,
        ruc
    });
}));
rucRoutes.get('/query', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let query = req.query.ruc;
    yield ruc_model_1.Ruc.find({ ruc: query })
        // .populate('ruc', 'ruc')
        .exec((err, ruc) => {
        if (err) {
            return res.json({
                ok: false,
                err
            });
        }
        res.json({
            ok: true,
            message: 'Conectado',
            ruc
        });
    });
}));
rucRoutes.get('/removeAll', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield ruc_model_1.Ruc.remove({});
    res.json({
        ok: true,
        message: 'Elimnado',
        result
    });
}));
rucRoutes.post('/', (req, res) => {
    const ruc = {
        ruc: req.body.ruc,
        nombre: req.body.nombre,
        dv: req.body.dv,
        ruc2: req.body.ruc2,
    };
    ruc_model_1.Ruc.create(ruc).then(rucDB => {
        res.json({
            ok: true,
            pais: rucDB
        });
    }).catch(err => {
        res.json({
            ok: false,
            err
        });
    });
});
rucRoutes.put('/:id', (req, res) => {
    let id = req.params.id;
    let body = req.body;
    ruc_model_1.Ruc.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err, rucDB) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            });
        }
        if (!rucDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El Id no es valido',
                }
            });
        }
        res.json({
            ok: true,
            pais: rucDB
        });
    });
});
exports.default = rucRoutes;
