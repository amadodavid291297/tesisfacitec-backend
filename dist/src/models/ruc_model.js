"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ruc = void 0;
const mongoose_1 = require("mongoose");
const rucSchema = new mongoose_1.Schema({
    ruc: {
        type: String,
        requiered: [true, 'El ruc es obligatorio']
    },
    dv: {
        type: String,
        requiered: [true, 'El dv es obligatorio']
    },
    nombre: {
        type: String,
        requiered: [true, 'El nombre es obligatorio']
    },
    ruc2: {
        type: String,
    }
});
exports.Ruc = (0, mongoose_1.model)('Ruc', rucSchema);
