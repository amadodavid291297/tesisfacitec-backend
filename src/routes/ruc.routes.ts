import { Router, Request, Response } from 'express';
import { Ruc } from '../models/ruc_model';
const rucRoutes = Router();

rucRoutes.get('/', async (req: Request, res: Response) => {
    let pagina = Number(req.query.pagina) || 1;
    let salto = Number(req.query.salto) || 18;
    let skip = pagina - 1;
    skip = skip * salto;
    const ruc = await Ruc.find()
        .sort({ _id: -1 })
        .skip(skip)
        .limit(salto)
        .exec();

    res.json({
        ok: true,
        pagina,
        ruc
    });
});

rucRoutes.get('/query', async (req: Request, res: Response) => {
    let query = req.query.ruc;

    await Ruc.find({ ruc: query })
        // .populate('ruc', 'ruc')
        .exec((err, ruc) => {
            if (err) {
                return res.json({
                    ok: false,
                    err
                });
            }
            res.json({
                ok: true,
                message: 'Conectado',
                ruc
            });
        });
})

rucRoutes.get('/removeAll', async (req: Request, res: Response) => {
    
  const result =  await Ruc.remove({});
  
    res.json({
        ok: true,
        message: 'Elimnado',
        result
    });
});

rucRoutes.post('/', (req: Request, res: Response) => {
    const ruc = {
        ruc: req.body.ruc,
        nombre: req.body.nombre,
        dv: req.body.dv,
        ruc2: req.body.ruc2,

    }

    Ruc.create(ruc).then(rucDB => {

        res.json({
            ok: true,
            pais: rucDB
        })
    }).catch(err => {
        res.json({
            ok: false,
            err
        })
    })
});

rucRoutes.put('/:id', (req: any, res: Response) => {
    let id = req.params.id;
    let body = req.body;
    Ruc.findByIdAndUpdate(id, body, { new: true, runValidators: true }, (err: any, rucDB: any) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err
            })
        }

        if (!rucDB) {
            return res.status(400).json({
                ok: false,
                err: {
                    message: 'El Id no es valido',
                }
            })
        }
        res.json({
            ok: true,
            pais: rucDB
        });
    })
})



export default rucRoutes;
