import express from 'express';
import cors from 'cors';

let port = process.env.PORT || 3000;
// let port = process.env.PORT ||

export default class Server {

    public app: express.Application;
    // public port :  process.env.PORT ||  3000;


    constructor() {
        this.app = express();
    }

    start(callback: Function) {
        this.app.listen(port, function () {
            console.log('Conectado al puerto -', port);
        });
    }


}