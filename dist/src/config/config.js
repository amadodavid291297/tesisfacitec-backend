"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// ============================
//  Puerto
// ============================
let port = process.env.PORT || 3000;
// ============================
//  Entorno
// ============================
process.env.NOD_ENV = process.env.NOD_ENV || 'dev';
// ============================
//  BD
// ============================
let urlDB;
if (process.env.NOD_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/tesisFacitec';
}
else {
    // console.log('valor process', process.env.NOD_ENV);
    urlDB = 'mongodb://localhost:27017/tesisFacitec';
}
class Config {
    constructor() {
        this.URL_DB = urlDB;
        this.PORT = port;
    }
}
exports.default = Config;
