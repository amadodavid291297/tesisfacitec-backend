"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// import mongoose from 'mongoose';
const mongoose_1 = __importDefault(require("mongoose"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const server_1 = __importDefault(require("./src/classes/server"));
const config_1 = __importDefault(require("./src/config/config"));
const ruc_routes_1 = __importDefault(require("./src/routes/ruc.routes"));
const config = new config_1.default;
const server = new server_1.default;
////////////////////////////
//body parser
server.app.use(body_parser_1.default.urlencoded({ extended: true }));
server.app.use(body_parser_1.default.json());
//Configuracion de Cood
server.app.use((0, cors_1.default)({ origin: true, credentials: true }));
// //Configuracion de Cood
// server.app.use(cors({ origin: true, credentials: true }));
//levantar express
server.start(() => {
    console.log(`Servidor corriendo en puerto ` + config.PORT);
});
// let mongodb = 'mongodb://localhost:27017/tesisFacitec';
let mongodb = 'mongodb+srv://facitec:facitecunican@cluster0.qd0vf.mongodb.net/?retryWrites=true&w=majority';
// let mongodb = 'mongodb+srv://monkeydb:lucasdavid07012018@cluster0.ngu9t.mongodb.net/monkeytv?retryWrites=true&w=majority';
// let mongodb = config.URL_DB;
//Rutas de la aplicacion
server.app.use('/ruc', ruc_routes_1.default);
mongoose_1.default.connect(mongodb + '', { //'mongodb://localhost:27017/latintv', {
// useNewUrlParser: true,
//  useCreateIndex: true
}, (error => {
    if (error)
        throw error;
    console.log('Base de datos ONLINE ', mongodb);
}));
