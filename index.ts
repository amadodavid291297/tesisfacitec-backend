// import mongoose from 'mongoose';
import mongoose, { ConnectOptions } from "mongoose";
import cors from 'cors';
import bodyParser from 'body-parser';
import Server from './src/classes/server';
import Config from './src/config/config';
import rucRoutes from './src/routes/ruc.routes';


const config = new Config;
const server = new Server;



////////////////////////////
//body parser
server.app.use(bodyParser.urlencoded({ extended: true }));
server.app.use(bodyParser.json());

//Configuracion de Cood
server.app.use(cors({ origin: true, credentials: true }));
// //Configuracion de Cood
// server.app.use(cors({ origin: true, credentials: true }));


//levantar express
server.start(() => {
    console.log(`Servidor corriendo en puerto ` + config.PORT);
});

// let mongodb = 'mongodb://localhost:27017/tesisFacitec';
let mongodb = 'mongodb+srv://facitec:facitecunican@cluster0.qd0vf.mongodb.net/?retryWrites=true&w=majority';
// let mongodb = 'mongodb+srv://monkeydb:lucasdavid07012018@cluster0.ngu9t.mongodb.net/monkeytv?retryWrites=true&w=majority';
// let mongodb = config.URL_DB;
//Rutas de la aplicacion
server.app.use('/ruc', rucRoutes);


mongoose.connect(mongodb + '', {//'mongodb://localhost:27017/latintv', {
    // useNewUrlParser: true,
    //  useCreateIndex: true
}, (error => {
    if (error) throw error;
    console.log('Base de datos ONLINE ', mongodb);
}))

