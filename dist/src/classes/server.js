"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
let port = process.env.PORT || 3000;
// let port = process.env.PORT ||
class Server {
    // public port :  process.env.PORT ||  3000;
    constructor() {
        this.app = (0, express_1.default)();
    }
    start(callback) {
        this.app.listen(port, function () {
            console.log('Conectado al puerto -', port);
        });
    }
}
exports.default = Server;
