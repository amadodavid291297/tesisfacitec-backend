import { model, Schema } from 'mongoose'
import { IRuc } from '../interfaces/ruc_interface';


const rucSchema = new Schema({
    ruc: {
        type: String,
        requiered: [true, 'El ruc es obligatorio']
    },
    dv: {
        type: String,
        requiered: [true, 'El dv es obligatorio']
    },
    nombre: {
        type: String,
        requiered: [true, 'El nombre es obligatorio']
    },
    ruc2: {
        type: String,
    }
});

export const Ruc = model<IRuc>('Ruc', rucSchema);