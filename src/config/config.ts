// ============================
//  Puerto
// ============================
let port = process.env.PORT || 3000;

// ============================
//  Entorno
// ============================

process.env.NOD_ENV = process.env.NOD_ENV || 'dev';


// ============================
//  BD
// ============================

let urlDB: string;

if (process.env.NOD_ENV === 'dev') {
    urlDB = 'mongodb://localhost:27017/tesisFacitec';
} else {
    // console.log('valor process', process.env.NOD_ENV);
    urlDB = 'mongodb://localhost:27017/tesisFacitec';
}



export default class Config {
    public URL_DB: string = urlDB;
    public PORT = port;
}